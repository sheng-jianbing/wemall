--
-- 表的结构 `addon_stores`
--

CREATE TABLE `addon_stores` (
  `id` int(10) unsigned NOT NULL,
  `name` text NOT NULL COMMENT '门店名',
  `username` text NOT NULL COMMENT '联系人',
  `tel` int(11) NOT NULL COMMENT '联系电话',
  `address` text NOT NULL COMMENT '门店地址',
  `status` int(11) NOT NULL COMMENT '1:开启0:关闭',
  `remark` text NOT NULL COMMENT '备注',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `addon_stores`
--

INSERT INTO `addon_stores` (`id`, `name`, `username`, `tel`, `address`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(1, '冰冰花店', '何冰华', 1008611, '郑州市金水区北三环园田路11号', 1, '', '2015-11-16 05:57:10', '0000-00-00 00:00:00'),
(2, '青青草店', '何青', 10001, '郑州市金水区北三环文化路73号', 1, '', '2015-11-17 22:07:19', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_stores`
--
ALTER TABLE `addon_stores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_stores`
--
ALTER TABLE `addon_stores`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;